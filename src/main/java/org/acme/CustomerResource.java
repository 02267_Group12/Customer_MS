package org.acme;

import dtu.clients.AccountManagementClient;
import dtu.clients.ReportClient;
import dtu.clients.TokenManagementClient;
import dtu.rs.entities.CreateTokenData;
import dtu.rs.entities.CustomerAccount;
import dtu.rs.entities.DTUPayAccount;
import dtu.rs.entities.Token;
import dtu.ws.fastmoney.Transaction;
import dtu.ws.fastmoney.User;
import entity.local.DTUTransaction;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Console;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Path("/customer")
public class CustomerResource {

    AccountManagementClient accountsClient = new AccountManagementClient();
    TokenManagementClient tokensClient = new TokenManagementClient();
    ReportClient reportClient = new ReportClient();
    static String baseUrl = "http://localhost:";
    static String port = "8080";
    List<Token> activeTokens = new ArrayList<Token>();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerNewCustomer(User user){
        System.out.println("before response");
        Response response = accountsClient.registerNewCustomer(user);
        if (response.getStatus() == 200){
            String cid = response.readEntity(String.class);
            return Response
                    .ok()
                    .location(URI.create(baseUrl + port + "/customer/" + cid))
                    .build();
        } else {
            return Response
                    .status(response.getStatus(), "Could not register user with cpr number: " + user.getCprNumber())
                    .entity("Could not register user with cpr number: " + user.getCprNumber())
                    .build();
        }
    }

    @GET
    @Path("/{cid}/report")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerReport(@PathParam("cid") String cid){
        Response response = reportClient.getCustomerReport(cid);
        if (response.getStatus() == 200){
            return response;
        } else {
            return Response
                    .status(response.getStatus(), "Failed to get all transactions from: " + cid)
                    .entity("Failed to get all transactions from: " + cid)
                    .build();
        }
    }

    @POST
    @Path("/{cid}/tokens")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTokensToCustomer(@PathParam("cid") String cid, int number){
        CreateTokenData tokenData = new CreateTokenData(cid, number);
        Response response = tokensClient.generateTokensForCustomer(tokenData);
        if (response.getStatus() == 200){
            activeTokens = response.readEntity(new GenericType<List<Token>>(){});
            return Response
                    .ok()
                    .entity(new GenericEntity<List<Token>>(activeTokens){})
                    .build();
        } else {
            return Response
                    .status(response.getStatus(),"Could not grant " + number + " tokens to user with id " + cid)
                    .entity("Could not grant " + number + " tokens to user with id " + cid)
                    .build();
        }
    }

    @GET
    @Path("/{cid}/tokens")
    public Response getTokensFromCustomer(@PathParam("cid") String cid){
        Response response = tokensClient.listTokens(cid);
        if (response.getStatus() == 200) {
            List<Token> tokens = response.readEntity(new GenericType<List<Token>>() {
            });
            return Response
                    .ok(new GenericEntity<List<Token>>(tokens){})
                    .build();
        }
        else{
            return Response
                    .status(response.getStatus(), "Failed to get tokens from user " + cid)
                    .entity("Failed to get tokens from user " + cid)
                    .build();
        }
    }


    @GET
    @Path("/{cid}")
    public Response getCustomerById(@PathParam("cid") String cid)
    {
        Response response = accountsClient.retrieveCustomerAccount(cid);
        if (response.getStatus() == 200) {
            DTUPayAccount account = response.readEntity(new GenericType<DTUPayAccount>() {});
            return Response
                    .ok(new GenericEntity<DTUPayAccount>(account){})
                    .build();
        } else {
            return Response
                    .status(response.getStatus(), "Failed to get customer wiht cid: " + cid)
                    .entity("Failed to get customer wiht cid: " + cid)
                    .build();
        }
    }



// @GET
// @Produces(MediaType.APPLICATION_JSON)
// @Path("/payments/{cpr}")
// public List<Transaction> getTransactionsFromCpr(@PathParam("cpr") String cpr){
//     //return manager.getTransactionsListFromDebtorCpr(cpr);
//     return new ArrayList<>();
// }

// @GET
// @Produces(MediaType.APPLICATION_JSON)
// @Path("/Token")
// public Token getToken(){
//     return new Token();
// }

// @GET
// @Produces(MediaType.APPLICATION_JSON)
// @Path("/getUser/{userID}")
// public CustomerAccount getUser(@PathParam("userID") String userID ){
//     return new CustomerAccount();
// }

// @GET
// @Produces(MediaType.APPLICATION_JSON)
// @Path("/getGeneratedReport")
// public List<Transaction> getGeneratedReport(){
//     return null;
// }

// @POST
// @Produces(MediaType.APPLICATION_JSON)
// public void postRefundPayment(){ }

}
