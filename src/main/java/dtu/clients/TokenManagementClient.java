package dtu.clients;

import dtu.rs.entities.CreateTokenData;
import dtu.rs.entities.Token;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


public class TokenManagementClient {

    private static String getRootPath(){
        String env = System.getenv("RUNNING_ENV");
        if (env == null) return "http://localhost:";
        else return "http://tokenmanagement:";
    }

    static String port = "5003/";
    static WebTarget baseUrl = ClientBuilder.newClient().target(getRootPath() + port);

    public Response listTokens(String cid) {
        Response response = baseUrl.path("tokens").path("customer").path(cid).request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
        return response;
    }

    public Response generateTokensForCustomer(CreateTokenData tokenData){

        Response response = baseUrl
                .path("tokens")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.entity(tokenData, MediaType.APPLICATION_JSON));
        return response;
    }

}
